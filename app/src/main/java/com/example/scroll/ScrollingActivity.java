package com.example.scroll;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ScrollingActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private ImageView fv_doctors_icon;
    private TextView fist_last_name,expertise,address,number_phone_one,number_phone_two,work_Experience;
    private Button btnturn ,btn_gettoken,show_all_doctor;
    private boolean flag = true;
    private ImageButton routing_img_btn;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //-----------------------------------------------------------------------------------------
        fist_last_name = findViewById(R.id.custom_layout_info_txt_body_name);
        expertise = findViewById(R.id.custom_layout_info_txt_body_expertise);
        address = findViewById(R.id.custom_layout_info_txt_body_address);
        number_phone_one = findViewById(R.id.custom_layout_info_txt_body_number_phone_one);
        number_phone_two = findViewById(R.id.custom_layout_info_txt_body_number_phone_two);
        work_Experience = findViewById(R.id.custom_layout_info_txt_body_work_Experience);
        btnturn = findViewById(R.id.btn);
        btn_gettoken = findViewById(R.id.btn_token);
        show_all_doctor = findViewById(R.id.btn_aa);
        fv_doctors_icon = findViewById(R.id.Activity_Info_Doctors_fv_icon);

        btnturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             vollaysingUp();
            }
        });
        btn_gettoken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vollayGetToken();
            }
        });
        show_all_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vollayuse();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //___________________________________________________________________________________________
    public void vollaysingUp() {
        Response.Listener<JSONObject> listener = new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Toast.makeText(ScrollingActivity.this, ""+response, Toast.LENGTH_SHORT).show();
                }
                catch (Exception e)
                {
                    Toast.makeText(ScrollingActivity.this, "4", Toast.LENGTH_SHORT).show();

                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        };
        JSONObject object = new JSONObject();
        try {
            object.put("username","rojin");
            object.put("password","12345");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest("http://172.31.100.58:8090/signup",object,listener,errorListener);
        AppController.getInstance().addToRequestQueue(request);

    }

    public void vollayGetToken() {
        Response.Listener<JSONObject> listener = new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    token=response.getString("token");

                    Toast.makeText(ScrollingActivity.this, ""+response, Toast.LENGTH_SHORT).show();

                }
                catch (Exception e)
                {
                    Toast.makeText(ScrollingActivity.this, "4", Toast.LENGTH_SHORT).show();

                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        };
        JSONObject object = new JSONObject();
        try {
            object.put("username","rojin");
            object.put("password","12345");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //**************************************************************************************


        //**************************************************************************************

        JsonObjectRequest request = new JsonObjectRequest( Request.Method.POST,"http://172.31.100.58:8090/token/generate-token",object,listener,errorListener){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        AppController.getInstance().addToRequestQueue(request);

    }

    public void vollayuse() {
        Response.Listener<JSONArray> listener = new Response.Listener<JSONArray>() {


            @Override
            public void onResponse(JSONArray response)
            {
                try
                {
                    Toast.makeText(ScrollingActivity.this, ""+response, Toast.LENGTH_SHORT).show();
                }
                catch (Exception e)
                {
                    Toast.makeText(ScrollingActivity.this, "4", Toast.LENGTH_SHORT).show();

                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        };


        //**************************************************************************************

        JsonArrayRequest request = new JsonArrayRequest( Request.Method.GET,"http://172.31.100.58:8090/new/doctor/showAll",null,listener,errorListener)
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer "+ token);
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(request);

    }
}
